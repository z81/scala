import scala.annotation.tailrec

object List {
  def empty[A]: List[A] = Nil
}

sealed trait List2[+T] {
  def ::[U >: T](x: U): List2[U] = new ::(x, this)

  def head : Option[T] = this match {
    case head :: _ => Some(head)
    case Nil2 => None
  }

  def tail : Option[List2[T]] = this match {
    case _ :: tail => Some(tail)
    case Nil2 => None
  }

  def foreach[B](clb: T => B): Unit = {
    @tailrec
    def rec(tail: List2[T]): Unit = tail match {
      case head :: t =>
        clb(head)
        rec(t)
      case Nil2 => ()
    }

    rec(this)
  }

  def reverse: List2[T] = {
    @tailrec
    def reverseTail(tail: List2[T], acc: List2[T]): List2[T] = tail match {
      case head :: t => reverseTail(t, head :: acc)
      case Nil2 => acc
    }
    reverseTail(this,  Nil2)
  }

  def foldLeft[B](init: B)(fn: (B, T) => B): B = {
    @tailrec
    def rec(list: List2[T], acc: B): B = list match {
      case head :: t => rec(t, fn(acc, head))
      case Nil2 => acc
    }

    rec(this, init)
  }


  def filter(pred: T => Boolean): List[T] =
    foldLeft(List.empty[T])((acc, it) => if (pred(it)) it :: acc else acc).reverse

  def join(delimiter: String): String = {
    def mk(acc: String, v: T) = {
      val delim = if (acc == "") "" else delimiter
      acc + delim + v
    }
    this.foldLeft("")(mk)
  }


//  def reduce[A >: T](fn: (T, T) => A): Option[A] = {
//    this match {
//      case head :: t => Some(t.foldLeft(head)(fn))
//      case Nil2 => None
//    }
//  }
}

final case class ::[T](hd: T, tl: List2[T]) extends List2[T]


case object Nil2 extends List2[Nothing]


object Main extends App {
  val list: List2[Int] = 1 :: 2 :: 3 :: Nil2
  val c = list.reverse
  val f = list.filter(_ > 1)
//  val red = list.reduce((x, y) => x max y)

//  println("reduce: ", red)
  println(c.join(","))
}
